package edu.unipampa.br.motusquiz.Model;

/**
 * Created by Bruno on 5/12/2017.
 */

public class Questions {
    String p1 = "Para você qual autor descreve melhor o significado de Liberdade?";
    String p2 = "Qual imagem lhe remete o sentimento de liberdade?";
    String p3 = "Liberdade, para você, é sinônimo de:";

    String opcao1Pergunta1 = " Liberdade de expressão é você ser livre para falar o que quiser, e eu ser livre para não querer te ouvir. "+"''Rodrigues, Geovani''";
    String opcao2Pergunta1 = " Acima de todas as liberdades, dê-me a de saber, de me expressar, de debater com autonomia, de acordo com minha consciência."+" ''Milton, John''";
    String opcao3Pergunta1 = " Liberdade é o direito de fazer tudo que as leis permitem." + " ''Montesquieu'' ";
    String opcao4Pergunta1 = " A liberdade só existe quando todos os nossos atos concordam com todo o nosso pensamento." + " ''Silva , Agostinho'' ";
    String opcao5Pergunta1 = " Ser pela liberdade não é apenas tirar as correntes de alguém, mas viver de forma que respeite e melhore a liberdade dos outros."+ " ''Mandela , Nelson'' ";

    String opcao1Pergunta3 = " Falar o que eu penso em qualquer momento.";
    String opcao2Pergunta3 = " Escolher o que eu quiser para a minha vida.";
    String opcao3Pergunta3 = " Ter a oportunidade de falar e a dignidade de ouvir.";
    String opcao4Pergunta3 = " Não ter que explicar minhas ações a ninguém.";
    String opcao5Pergunta3 = " Optar por caminhos a seguir e arcar com as consequências";


    String r1 = "Folhas amarelas, esgoto letrado\n de Paulo Ismar Mota Florindo \n" +
            "Na tevê, misérias\n" +
            "nos jornais,\n" +
            "meios banais\n" +
            "de noticiar a morte\n" +
            "desta vida encarcerada\n" +
            "de caras enceradas\n" +
            "pelo matiz da mentira\n" +
            " \n" +
            "Nos lides e lidas\n" +
            "muitas lambidas\n" +
            "muitos gemidos\n" +
            "espremidos em tinta\n" +
            "papel jornal,\n" +
            "letras, palavras jogral\n" +
            "em golpe germinal\n" +
            " \n" +
            "Das novas verdades\n" +
            "às antigas mentiras\n" +
            "novos formatos\n" +
            "para antigas máximas\n" +
            "em mínimas medidas\n" +
            "o que resta é o sentido\n" +
            "há muito invertido\n" +
            " \n" +
            "Das folhas brancas\n" +
            "encardidas matérias\n" +
            "nada etéreas\n" +
            "nada profundas\n" +
            "conteúdo incontido\n" +
            "em doses cavalares\n" +
            "de canalhice explícita\n" +
            " \n" +
            "Abriu-se, fechou-se\n" +
            "o esgoto escorre\n" +
            "palavras degradadas\n" +
            "letras mortas\n" +
            "idioma corrompido\n" +
            "verdade agredida\n" +
            "informação assassinada.\n";

    String r2 = "Poema ativista\n Guilherme Ferreira Aniceto  \n" +
            " \n" +
            "Meu poema é ativista,\n" +
            "não perde o bonde do protesto\n" +
            "e não para diante do monte,\n" +
            "prossegue e diz - prossiga!\n" +
            "Meu poema a voz eleva\n" +
            "e grita seus desatinos,\n" +
            "como se doesse no âmago\n" +
            "um soco no estômago.\n" +
            "Meu poema é distinto\n" +
            "nas rodas e nos terreiros,\n" +
            "não tem medo de entidades,\n" +
            "tampouco teme divindades.\n" +
            "Meu poema é livre e opera\n" +
            "a transformação da fera\n" +
            "que, de pequena oprimida,\n" +
            "desabrocha empoderada.\n" +
            "Meu poema diz à gente\n" +
            "que sob as botas do mundo\n" +
            "age feito barata - não tema,\n" +
            "a vida nem sempre mata!\n" +
            "Meu poema é ativista,\n" +
            "não se prende na revista\n" +
            "ou ao padrão normativo,\n" +
            "hetero, cis, branco, elitista.\n" +
            "Meu poema cai na estrada,\n" +
            "mata a fome da romaria,\n" +
            "converte a vista turva e fanática\n" +
            "em uma visão mais fantástica.\n" +
            "Meu poema protege a pele,\n" +
            "o belo cabelo, o grito bonito\n" +
            "dos confins mais sem fim\n" +
            "das favelas e perifas.\n" +
            "Meu poema é dono de si,\n" +
            "da voz que empresta,\n" +
            "do corpo que veste e\n" +
            "da boca com que beija.\n" +
            "\n";
String r3 = "Manual de Moda do Poeta\n Thiago Teixeira Scarlata \n" +
        "\n" +
        "para começar\n" +
        "nenhum poeta deve usar\n" +
        "terno em qualquer recinto\n" +
        "isso endurece o poema dormido\n" +
        " \n" +
        "a gravata, seguindo isto\n" +
        "nem em formalidades\n" +
        "enforca gargantas\n" +
        "e alguma frase\n" +
        " \n" +
        "um chapéu talvez\n" +
        "não seja tão necessário\n" +
        "abafa a cuca e a um poeta\n" +
        "isso é contra-indicado\n" +
        " \n" +
        "o colete e o corpete\n" +
        "apertam muito, até demais\n" +
        "deixam o peito do arteiro\n" +
        "impedido de arejar\n" +
        " \n" +
        "cinto e suspensórios\n" +
        "são totalmente dispensáveis\n" +
        "pois não havendo calças\n" +
        "ficam sem utilidade\n" +
        " \n" +
        "um vestido ou uma saia\n" +
        "dependendo do fecho\n" +
        "prejudicam o caminhar\n" +
        "favorecendo o tropeço\n" +
        "\n" +
        "\n" +
        "\n" +
        "a camisa encarde logo\n" +
        "a camiseta ainda mais rápido\n" +
        "e sendo o poeta tropical\n" +
        "o despir fica mais fácil\n" +
        " \n" +
        "sandálias, saltos e sapatos\n" +
        "sufocam toda sensibilidade\n" +
        "de pés atentos de tato\n" +
        "além de doerem os calos\n" +
        " \n" +
        "se está liberada a calcinha,\n" +
        "a cueca ou outra roupa de baixo?\n" +
        "do pudor o poeta despe-se\n" +
        "do que tampa a intimidade\n" +
        "\n" +
        "\n" +
        "concluindo este manual\n" +
        "fica aqui notificado\n" +
        "todo o bom poeta deve\n" +
        "transitar sempre pelado \n" +
        " \n";


    String r4 = "Essa tal liberdade\n  Erivania dos Santos Fernandes\n \n"  +
            "Quisera poder voar sobre as nuvens\n" +
            "E alcançar o universo\n" +
            "Quisera eu ser tudo aquilo\n" +
            "Que meu coração almeja ser.\n" +
            " \n" +
            "Quisera eu ser livre\n" +
            "E compor uma melodia\n" +
            "Para essa tal liberdade.\n" +
            " \n" +
            "Quisera poder voar sobre as nuvens,\n" +
            "E alcançar o universo.\n" +
            "Quisera eu ser tudo aquilo\n" +
            "Que meu coração almeja ser.\n" +
            " \n" +
            "Quisera eu ser livre,\n" +
            "Para compor a própria canção\n" +
            "Do meu cansado coração.\n" +
            " \n" +
            "Ouço homens falando em liberdade\n" +
            "Mas onde está essa tal liberdade,\n" +
            "Se nossa expressão é silenciada\n" +
            "E nossa alma cala?\n" +
            " \n" +
            "Onde está a liberdade da rua\n" +
            "Dos becos e vielas?\n" +
            "Onde está o porto das verdades,\n" +
            "Que os navegantes viajam \n" +
            "Até chegar ao cais das palavras,\n" +
            "Submersas no mar da alma?\n" +
            " \n" +
            "A opinião grita\n" +
            "E os pensamentos clamam.\n" +
            "Deixemos que o mundo conheça,\n" +
            "O poder das ideias que ressurgem\n" +
            "Ao fim do crepúsculo,\n" +
            "Que brotam a luz da aurora.\n" +
            " \n" +
            "Vamos planar sobre o céu\n" +
            "Da nossa profunda expressão,\n" +
            "E deixar que as águas da liberdade\n" +
            "Alcancem a muralha,\n" +
            "Da indiferença humana.\n";

    String r5 = "DOMA TUA LÍNGUA!\n Felipe Couto Lima\n \n" +
            "Doma tua língua! Guarda-a bem contigo!\n" +
            "Não te enganes a ti com o que diz,\n" +
            "Pois dos órgãos todos é a meretriz,\n" +
            "Diacho indefesso não nos seja amigo!\n" +
            " \n" +
            "Do homem o mais maléfico inimigo!\n" +
            "Cuide não crê-la; pois mais de ti diz,\n" +
            "Quando doutros fala. É bem mais feliz\n" +
            "Quem mais se cala. Pois foge o castigo,\n" +
            " \n" +
            "Se te encantas pela sabedoria.\n" +
            "Reflita teu dito e estejas atento.\n" +
            "Morem tuas posses na filosofia,\n" +
            " \n" +
            "E nela, somente, tenhas teu alento!\n" +
            "Nova triste de ti não venha um dia:\n" +
            "Jaz jus a língua – envenenamento.\n" +
            " \n";

    String r6 ="ENGARRAFAR-ME?\n  Julieta de Souza\n" +
            " \n" +
            "Quisera eu caber apenas dentro desta garrafa,\n" +
            "moldar-me à sua forma e sentir-me realizada.\n" +
            " \n" +
            "Quisera eu reprimir minha alma para aceitar sua imposição,\n" +
            "encolher minhas verdades para inebriar-me com a suas vaidades!\n" +
            " \n" +
            "Quisera eu conter meus desprendimentos para deleitar-me com as suas ambições,\n" +
            "compactar minha pureza para saborear sua lascívia,\n" +
            "contrair os meus tesouros para degustar suas futilidades.\n" +
            " \n" +
            "Quisera eu comprimir minhas ponderações para me embriagar com seus impulsos,\n" +
            "sintetizar meu equilíbrio para saborear seus exageros.\n" +
            " \n" +
            "Quisera eu caber apenas dentro desta garrafa!\n" +
            "Meu mundo seria então simples e finito.\n" +
            "E eu não teria mais motivos para escrever.\n";

    String r7 = "Liberdade de Expressão\n  Fabio Luís Vasques Silva\n" +
            "\n" +
            "Hesitante,\n" +
            "Caminha o orador,\n" +
            "Ainda iniciante,\n" +
            "Ao centro da praça,\n" +
            "E das atenções.\n" +
            "\n" +
            "O caixote,\n" +
            "Parlatório improvisado,\n" +
            "Se agiganta,\n" +
            "Em proporção geométrica,\n" +
            "Diante dos olhos miúdos,\n" +
            "E dos sussurros, ensurdecedores,\n" +
            "Da multidão.\n" +
            "\n" +
            "Mas um rabecão,\n" +
            "Que cruza, de mansinho,\n" +
            "A esquina próxima,\n" +
            "Desperta um gigante no menino:\n" +
            "A vontade de viver,\n" +
            "Do tamanho da fome,\n" +
            "Que lhe corrói as entranhas.\n" +
            "\n" +
            "Então,\n" +
            "Convencido pelo acaso,\n" +
            "Insiste na ocasião:\n" +
            "Malabarista do cotidiano,\n" +
            "Finca os pés pequenos,\n" +
            "Descalços e empoeirados,\n" +
            "À beira do palco imaginário.\n" +
            " \n" +
            "E, aprumando a voz miúda,\n" +
            "Reverbera ao microfone inventado.\n" +
            "\n" +
            "Inaugura a fala com uma revelação,\n" +
            "Tão simples quanto profunda:\n" +
            "\"Eu sou o João\".\n" +
            "\n" +
            "E a sentença,\n" +
            "Qual tiro certo,\n" +
            "Implode a indiferença\n" +
            "[entrincheirada nos olhares.\n" +
            "\n" +
            "Surpreso,\n" +
            "Diante do resultado atômico,\n" +
            "O mensageiro iônico,\n" +
            "Carrega, aos sorrisos,\n" +
            "A esperança depositada,\n" +
            "Aos vinténs,\n" +
            "Num chapéu de palha rota.\n" +
            "\n" +
            "Restou, com o tempo,\n" +
            "O testemunho de um mendigo,\n" +
            "Que a tudo, há muito, assistira:\n" +
            "Da palavra que liberta,\n" +
            "Resgatada pelo poema,\n" +
            "Que perpetua e fertiliza,\n" +
            "Multiplicada,\n" +
            "Por infinitas gerações.\n";

    String r8 = "SEMPRE FOI UM JOGO\n Paulo Eduardo Porto Caldeira" +
            " \n" +
            " \n" +
            "Parte I\n" +
            " \n" +
            "Minha vida sempre foi um jogo.\n" +
            "Achei que eu conseguiria\n" +
            "montar esse quebra-cabeça\n" +
            "até descobrir que eu não seria\n" +
            "nada além de mais uma peça.\n" +
            "O sistema não vai me controlar.\n" +
            " \n" +
            "Vou seguir meus instintos.\n" +
            "Sempre fui meu psicólogo,\n" +
            "meu gerente, meu doutor.\n" +
            "Sempre fui meu astrólogo,\n" +
            "meu líder, manipulador.\n" +
            "O sistema não vai me controlar.\n" +
            " \n" +
            "Vou seguir meus instintos.\n" +
            "Compartilhar bons momentos,\n" +
            "curtir de verdade, como antes,\n" +
            "com 3 amigos ao invés de 300,\n" +
            "tão simples como diamantes.\n" +
            "Minha vida sempre foi um jogo.\n" +
            " \n" +
            " \n" +
            "Parte II\n" +
            " \n" +
            "Minha vida sempre foi um jogo.\n" +
            "Não tenho monets na parede\n" +
            "nem mercedez na garagem,\n" +
            "não matariam minha sede\n" +
            "nem me dariam mais coragem.\n" +
            "O sistema não vai me controlar.\n" +
            " \n" +
            "Vou seguir meus instintos.\n" +
            "Sempre fui meu estilista,\n" +
            "meu advogado, meu pastor.\n" +
            "Sempre fui meu roteirista \n" +
            "meu carrasco, contraditor.\n" +
            "O sistema não vai me controlar.\n" +
            " \n" +
            "Vou seguir meus instintos.\n" +
            "Nunca vou dar às costas,\n" +
            "a cada rodada uma surpresa,\n" +
            "posso fazer minhas apostas\n" +
            "ou simplesmente virar a mesa.\n" +
            "Minha vida sempre foi um jogo.\n" +
            " \n";
    String r9 = "No que expresso, eu estou\n Olidnéri Bello" +
            " \n" +
            "Expresso-me na cor de minha pela;\n" +
            "Na roupa que uso; na língua que falo;\n" +
            "No ideal do sonho, onde não há quem me atropele;\n" +
            "Na imensidão do cosmo, onde não me calo.\n" +
            " \n" +
            "Expresso-me no percurso de meu cochilo,\n" +
            "No qual me perco e me encontro\n" +
            "Num infinito caminho abençoado e tranquilo,\n" +
            "Pois nele está a Força Superior em que me concentro.\n" +
            " \n" +
            "Expresso-me na luta diária\n" +
            "Pelo pão, pela justiça, pela compreensão,\n" +
            "Por uma vida plena e igualitária,\n" +
            "Sem ideias de minorias, liberta e em plena redenção.\n" +
            " \n" +
            "Liberto-me quando ignoro a injúria;\n" +
            "Quando não encontro a maldade;\n" +
            "Quando, da inveja, esmago a fúria;\n" +
            "Quando administro a inimizade.\n" +
            " \n" +
            "Liberto-me quando coloco sob meus pés\n" +
            "O conjunto infame de preconceitos;\n" +
            "Os olhares de onde parte todo o revés;\n" +
            "A pretensa superioridade em atos não aceitos.\n" +
            " \n" +
            "Liberto-me quando grito quem eu sou ao mundo,\n" +
            "Nem maior, nem menor que ninguém,\n" +
            "Mas dotada de identidade própria e respeito profundo\n" +
            "À liberdade de expressão: manifesto justo de quem anseia o bem.\n";

    String r10 = " Revolta\n Maria Apparecida Sanches Coquemala" +
            " \n" +
            "Manhã, fria, chuvosa...\n" +
            "Ônibus lotados. \n" +
            "Capas, guarda-chuvas, \n" +
            "botas, chinelos, sapatos velhos\n" +
            "de pedestres apressados\n" +
            "rumando ao trabalho,\n" +
            "se confundem na calçada.\n" +
            "Centenas de táxis, de carros\n" +
            "vêm e vão de todo lado...\n" +
            "Lugares vagos contrastam \n" +
            "com os de ônibus lotados.\n" +
            " \n" +
            "De repente, na rua, na chuva, \n" +
            "o homem grita e gesticula adoidado:\n" +
            "- É um absurdo, estamos todos cegos?\n" +
            "Todos surdos? Todos mudos?\n" +
            "Esquecemos mil setecentos e oitenta e nove?\n" +
            "Nossas bastilhas se encheram \n" +
            "de marginalizados, de injustiçados,\n" +
            "enquanto locupletam  os bolsos\n" +
            "empresários, políticos e apadrinhados!\n" +
            "E ninguém vê? Ninguém age?\n" +
            "E se o povo não tem pão que coma o quê?\n" +
            "Cadê nossa guilhotina, nosso Robespierre?\n" +
            "Morte pra essas  antonietas  nababescas!\n" +
            "Morte pra essa cambada de ladrões do povo!\n" +
            " \n" +
            "A Polícia chegou e agarrando-o \n" +
            "truculenta, o arrastou a uma bastilha, \n" +
            "onde rápido se juntou ao PCC.\n" +
            "Enquanto na rua gritava a multidão:\n" +
            "- Cadê nossa liberdade de expressão?\n";


    int  resultado;


    public String getR1() {
        return r1;
    }

    public void setR1(String r1) {
        this.r1 = r1;
    }

    public String getR2() {
        return r2;
    }

    public void setR2(String r2) {
        this.r2 = r2;
    }

    public String getR3() {
        return r3;
    }

    public void setR3(String r3) {
        this.r3 = r3;
    }

    public String getOpcao1Pergunta3() {
        return opcao1Pergunta3;
    }

    public void setOpcao1Pergunta3(String opcao1Pergunta3) {
        this.opcao1Pergunta3 = opcao1Pergunta3;
    }

    public String getOpcao2Pergunta3() {
        return opcao2Pergunta3;
    }

    public void setOpcao2Pergunta3(String opcao2Pergunta3) {
        this.opcao2Pergunta3 = opcao2Pergunta3;
    }

    public String getOpcao3Pergunta3() {
        return opcao3Pergunta3;
    }

    public void setOpcao3Pergunta3(String opcao3Pergunta3) {
        this.opcao3Pergunta3 = opcao3Pergunta3;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public String getP1() {
        return p1;
    }

    public void setP1(String p1) {
        this.p1 = p1;
    }

    public String getP2() {
        return p2;
    }

    public void setP2(String p2) {
        this.p2 = p2;
    }

    public String getP3() {
        return p3;
    }

    public void setP3(String p3) {
        this.p3 = p3;
    }

    public String getOpcao1Pergunta1() {
        return opcao1Pergunta1;
    }

    public void setOpcao1Pergunta1(String opcao1Pergunta1) {
        this.opcao1Pergunta1 = opcao1Pergunta1;
    }

    public String getOpcao2Pergunta1() {
        return opcao2Pergunta1;
    }

    public void setOpcao2Pergunta1(String opcao2Pergunta1) {
        this.opcao2Pergunta1 = opcao2Pergunta1;
    }

    public String getOpcao3Pergunta1() {
        return opcao3Pergunta1;
    }

    public void setOpcao3Pergunta1(String opcao3Pergunta1) {
        this.opcao3Pergunta1 = opcao3Pergunta1;
    }

    public String getOpcao4Pergunta1() {
        return opcao4Pergunta1;
    }

    public void setOpcao4Pergunta1(String opcao4Pergunta1) {
        this.opcao4Pergunta1 = opcao4Pergunta1;
    }

    public String getOpcao5Pergunta1() {
        return opcao5Pergunta1;
    }

    public void setOpcao5Pergunta1(String opcao5Pergunta1) {
        this.opcao5Pergunta1 = opcao5Pergunta1;
    }

    public String getOpcao4Pergunta3() {
        return opcao4Pergunta3;
    }

    public void setOpcao4Pergunta3(String opcao4Pergunta3) {
        this.opcao4Pergunta3 = opcao4Pergunta3;
    }

    public String getOpcao5Pergunta3() {
        return opcao5Pergunta3;
    }

    public void setOpcao5Pergunta3(String opcao5Pergunta3) {
        this.opcao5Pergunta3 = opcao5Pergunta3;
    }

    public String getR4() {
        return r4;
    }

    public void setR4(String r4) {
        this.r4 = r4;
    }

    public String getR5() {
        return r5;
    }

    public void setR5(String r5) {
        this.r5 = r5;
    }

    public String getR6() {
        return r6;
    }

    public void setR6(String r6) {
        this.r6 = r6;
    }

    public String getR7() {
        return r7;
    }

    public void setR7(String r7) {
        this.r7 = r7;
    }

    public String getR8() {
        return r8;
    }

    public void setR8(String r8) {
        this.r8 = r8;
    }

    public String getR9() {
        return r9;
    }

    public void setR9(String r9) {
        this.r9 = r9;
    }

    public String getR10() {
        return r10;
    }

    public void setR10(String r10) {
        this.r10 = r10;
    }
}
