package edu.unipampa.br.motusquiz.Controller;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import edu.unipampa.br.motusquiz.R;

public class SplashScreen extends AppCompatActivity implements Runnable {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Handler handler = new Handler();
        handler.postDelayed(this,2000); // utilização do handler para deixar o logo aparecendo por 2 segundos

    }

    /*
    Método responsável por chamar a tela ConsultarProcesso
     */
    @Override
    public void run() {
        startActivity(new Intent(this, PerguntaUm.class));
        finish();
    }
}
