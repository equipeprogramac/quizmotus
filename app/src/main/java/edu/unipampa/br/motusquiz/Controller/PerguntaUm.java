package edu.unipampa.br.motusquiz.Controller;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import edu.unipampa.br.motusquiz.Model.Questions;
import edu.unipampa.br.motusquiz.R;

public class PerguntaUm extends AppCompatActivity {
    int points;
    Questions questions = new Questions();
    ImageButton ok;
    RadioGroup rg1;
    RadioButton rb1, rb2, rb3, rb4, rb5;
    TextView pergunta1, rodape;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pergunta_um);


        pergunta1 = (TextView) findViewById(R.id.textViewPergunta1);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
        pergunta1.setTypeface(font);
        pergunta1.setText(questions.getP1());

        rb1 = (RadioButton) findViewById(R.id.radioButtonP1);
        rb1.setText(questions.getOpcao1Pergunta1());
        Typeface fontRb1 = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Book.otf");
        rb1.setTypeface(fontRb1);
        rb2 = (RadioButton) findViewById(R.id.radioButton2P1);
        Typeface fontRb2 = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Book.otf");
        rb2.setTypeface(fontRb2);
        rb2.setText(questions.getOpcao2Pergunta1());
        rb3 = (RadioButton) findViewById(R.id.radioButton3P1);
        Typeface fontRb3 = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Book.otf");
        rb3.setTypeface(fontRb3);
        rb3.setText(questions.getOpcao3Pergunta1());
        rb4 = (RadioButton) findViewById(R.id.radioButton4P1);
        Typeface fontRb4 = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Book.otf");
        rb4.setTypeface(fontRb4);
        rb4.setText(questions.getOpcao4Pergunta1());
        rb5 = (RadioButton) findViewById(R.id.radioButton5P1);
        Typeface fontRb5 = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Book.otf");
        rb5.setTypeface(fontRb5);
        rb5.setText(questions.getOpcao5Pergunta1());


        rodape = (TextView) findViewById(R.id.rodapeText1);
        Typeface fontRodape = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
        rodape.setTypeface(fontRodape);
        rodape.setText("Motus - Programa C");


        rg1 = (RadioGroup) findViewById(R.id.rg1);
        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int i) {
                switch (i) {

                    case R.id.radioButtonP1:
                        points = 1;
                        questions.setResultado(points);
                        break;
                    case R.id.radioButton2P1:
                        points = 2;
                        questions.setResultado(points);
                        break;
                    case R.id.radioButton3P1:
                        points = 3;
                        questions.setResultado(points);
                        break;
                    case R.id.radioButton4P1:
                        points = 4;
                        questions.setResultado(points);
                        break;
                    case R.id.radioButton5P1:
                        points = 5;
                        questions.setResultado(points);
                        break;
                    }

                       }
        });


        /**/

    }

    private void exibirMensagem(String mensagem){
        Context contexto = getApplicationContext();
        int duracao = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(contexto, mensagem, duracao);
        toast.show();
    }

    public void enviarDados(Intent it) {
        it.putExtra("respostaPrimeiraPergunta", questions.getResultado());

    }
    public void irParaPergunta2(View v){
        if(rb1.isChecked()== false && rb2.isChecked()== false && rb3.isChecked() == false && rb4.isChecked() == false && rb5.isChecked() == false){
            String texto3 = "Selecione alguma opção!";
            exibirMensagem(texto3);
        }else{
            Intent i = new Intent(this, PerguntaDois.class);
            enviarDados(i);
            startActivity(i);
        }
    }
}
