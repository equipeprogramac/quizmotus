package edu.unipampa.br.motusquiz.Controller;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import edu.unipampa.br.motusquiz.Model.Questions;
import edu.unipampa.br.motusquiz.R;

public class PerguntaDois extends AppCompatActivity {
    Questions questions = new Questions();
    int resultado_aux;
    int resultado1;
    Bundle bundle;
    TextView pergunta2, rodape;
    int points;
    RadioGroup rg2;
    RadioButton rb1,rb2,rb3, rb4, rb5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pergunta_dois);

        pergunta2 = (TextView) findViewById(R.id.textViewPergunta2);
        Typeface p2 = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
        pergunta2.setTypeface(p2);
        pergunta2.setText(questions.getP2());

        bundle = getIntent().getExtras();

        rb1 = (RadioButton) findViewById(R.id.radioButtonP2);

        rb2 = (RadioButton) findViewById(R.id.radioButton2P2);

        rb3 = (RadioButton) findViewById(R.id.radioButton3P2);

        rb4 = (RadioButton) findViewById(R.id.radioButton4P2);

        rb5 = (RadioButton) findViewById(R.id.radioButton5P2);


        rodape = (TextView) findViewById(R.id.rodapeText2);
        Typeface fontRodape = Typeface.createFromAsset(getAssets(), "fonts/Georgina.ttf");
        rodape.setTypeface(fontRodape);
        rodape.setText("Motus - Programa C");


        rg2 = (RadioGroup) findViewById(R.id.rg2);
        rg2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int i) {
            switch (i) {

                case R.id.radioButtonP2:
                    points = 1;
                    questions.setResultado(points);
                    break;
                case R.id.radioButton2P2:
                    points = 2;
                    questions.setResultado(points);
                    break;
                case R.id.radioButton3P2:
                    points = 3;
                    questions.setResultado(points);
                    break;
                case R.id.radioButton4P2:
                    points = 4;
                    questions.setResultado(points);
                    break;
                case R.id.radioButton5P2:
                    points = 5;
                    questions.setResultado(points);
                    break;

            }

        }
    });



    }

    private void exibirMensagem(String mensagem){
        Context contexto = getApplicationContext();
        int duracao = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(contexto, mensagem, duracao);
        toast.show();
    }

    public void enviarDados(Intent it) {

        resultado1 = bundle.getInt("respostaPrimeiraPergunta");
        resultado_aux = questions.getResultado() + resultado1;
        it.putExtra("respostaSegundaPergunta", resultado_aux);


    }

    public void buttonVoltarPara1(View v){
        Intent i = new Intent(PerguntaDois.this, PerguntaUm.class);
        startActivity(i);
    }

    public void irParaPergunta3(View v){
        if(rb1.isChecked()== false && rb2.isChecked()== false && rb3.isChecked() == false && rb4.isChecked() == false && rb5.isChecked() == false){
            String texto3 = "Selecione alguma opção!";
            exibirMensagem(texto3);
        }else{
            Intent i = new Intent(PerguntaDois.this, PerguntaTres.class);
            enviarDados(i);
            startActivity(i);
        }
    }
    }


